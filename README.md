# My Void User Repository

This is my attempt at making a repository with custom templates play well with `xbps-src`. It can be used manually or with the help of [mvur](https://codeberg.org/Eloitor/mvur "minimal VUR helper").

1. Set up a normal void-packages repo.

```
git clone --depth=1 https://github.com/void-linux/void-packages.git
cd void-packages
./xbps-src binary-bootstrap
sudo xbps-install -S xtools
```

2. Clone this repo with `--bare` inside void-packages.

```
git clone --bare https://codeberg.org/Eloitor/VUR.git
git --git-dir VUR.git --work-tree srcpkgs checkout
```

3. Now you can build any package in this repo as any regular package in void-packages. For example:

```
./xbps-src pkg ytfzf
xi ytfzf
```

Optionally you can set an alias in your `~/.bashrc` as follows:

```
alias vur="git --git-dir VUR.git --work-tree srcpkgs"
```

This allows the use of `vur pull` to get the latest templates :)



## Installation of feedbackd

Add the following line to `common/shlibs` before installing feedbackd
```
libfeedback-0.0.so.0 libfeedback-0.0.0+20220208_1
```
